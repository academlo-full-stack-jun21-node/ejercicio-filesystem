//Imports de los módulos
//fs y path

const getUsers = async() => {
    //Leer el archivo

    //Regresar el arreglo de usuarios como objeto literal Javascript (sin notación JSON)
};

const addUser = async (userObj) => {
    //leer el archivo 

    //convertir el contenido en formato JSON en un objeto JS

    //agregar el usuario en el arreglo

    //sobreescribir el arreglo en el archivo

    //si el proceso se realizó correctamente tendrás que regresar el objeto de usuario
    //que acabas de agregar en el arreglo
};

const clearUsers = async () => {
    //Vaciar el arreglo y sobreescribir el archivo
    
    //Si el proceso se realizó correctamente tendrás que regresar true
}

module.exports = {
    getUsers,
    addUser,
    clearUsers,
};
